from io_terminal import imprime_lista

nome_ficheiro_lista_de_trotinetas = "lista_de_trotinetas.pk"


def cria_trotineta():
    """ Pede ao utilizador para introduzir uma nova trotineta

    :return: dicionario com uma trotineta na forma 
        {"marca": <<marca>>, "modelo": <<modelo>>, "matrícula": <<matricula>>}
    """


    marca = input("Qual a marca? ")
    modelo = input("Qual o modelo? ").upper()
    matricula = input("Qual a matricula")
    return {"marca": marca, "modelo": modelo, "matrícula": matricula}


def imprime_lista_de_trotinetas(lista_de_veiculos):
    """ Imprime lista de trotinetas

    :param cabecalho: cabecalho para a listagem
    :param lista: lista a ser impressa
    """

    imprime_lista(cabecalho="Lista de Veiculos", lista=lista_de_veiculos)

