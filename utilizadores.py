from io_terminal import imprime_lista

nome_ficheiro_lista_de_utilizadores = "lista_de_utilizadores.pk"

def cria_novo_utilizador():
    """ Pede os dados de um novo utilizador

    :return: dicionario com o nome e email do utilizador na forma,
        {"nome": <<nome>>, "email": <<email>>}
    """

    nome = input("Qual o nome? ")
    email = input("Qual o email? ")
    return {"nome": nome, "email": email}


def imprime_lista_de_utilizadores(lista_de_utilizadores):
    """ Imprime lista de utilizadores

    :param cabecalho: cabecalho para a listagem
    :param lista: lista a ser impressa
    """

    imprime_lista(cabecalho="Lista de Utilizadores", lista=lista_de_utilizadores)
