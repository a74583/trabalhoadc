var namespaces_dup =
[
    [ "aluguer", "namespacealuguer.html", [
      [ "cria_novo_aluguer", "namespacealuguer.html#a76bf7f7ba2f782266347d3c87fe8c979", null ],
      [ "imprime_lista_de_alugueres", "namespacealuguer.html#a3cf731c10e0217328ad12cb6db3de2c8", null ],
      [ "nome_ficheiro_lista_de_alugueres", "namespacealuguer.html#a39e4f7d94bb3195861ef517f3a7f7113", null ]
    ] ],
    [ "io_ficheiros", "namespaceio__ficheiros.html", [
      [ "guarda_em_ficheiro", "namespaceio__ficheiros.html#a369644d3191c28b8e2cab4b338c0645b", null ],
      [ "le_de_ficheiro", "namespaceio__ficheiros.html#ab73dab7ce2592fb7e81dfdadcbd70124", null ]
    ] ],
    [ "io_terminal", "namespaceio__terminal.html", [
      [ "imprime_lista", "namespaceio__terminal.html#a7585a7a42b845411093fdc37067250bb", null ],
      [ "imprime_lista_de_dicionarios", "namespaceio__terminal.html#af8d306b126599f43b142967e3dd241d3", null ]
    ] ],
    [ "main", "namespacemain.html", [
      [ "carrega_as_listas_dos_ficheiros", "namespacemain.html#a871dd5c3fa3b04adc869b4fcc54e30f8", null ],
      [ "guarda_as_listas_em_ficheiros", "namespacemain.html#ab15e8e0828349ab82b432084737b1236", null ],
      [ "menu", "namespacemain.html#afa51d8c0e4aa208f8b4454743e793774", null ],
      [ "pergunta_id", "namespacemain.html#a6a1c769386caabed12d14f2e8b84da48", null ]
    ] ],
    [ "trotinetas", "namespacetrotinetas.html", [
      [ "cria_trotineta", "namespacetrotinetas.html#afd7b17b2357034ab1002efa95cc388f4", null ],
      [ "imprime_lista_de_trotinetas", "namespacetrotinetas.html#acaeb7c8ab261426cd7d4f7d8d8951e95", null ],
      [ "nome_ficheiro_lista_de_trotinetas", "namespacetrotinetas.html#aa33739a76004db9da29553f9a2a50490", null ]
    ] ],
    [ "utilizadores", "namespaceutilizadores.html", [
      [ "cria_novo_utilizador", "namespaceutilizadores.html#a337c5dcc68092c12dde3d8c8b3dfdc7a", null ],
      [ "imprime_lista_de_utilizadores", "namespaceutilizadores.html#a5450fd486e3021184a8b3fa35f8311bb", null ],
      [ "nome_ficheiro_lista_de_utilizadores", "namespaceutilizadores.html#acbdc556b7228de39c0ea9b3150395341", null ]
    ] ]
];