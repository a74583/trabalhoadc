/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My ADC Project", "index.html", [
    [ "Projeto Universidade do Algarve", "md__r_e_a_d_m_e.html", [
      [ "New Feature", "md__r_e_a_d_m_e.html#autotoc_md1", null ],
      [ "License", "md__r_e_a_d_m_e.html#autotoc_md2", null ]
    ] ],
    [ "Pacotes", "namespaces.html", [
      [ "Pacotes", "namespaces.html", "namespaces_dup" ],
      [ "Funções do Pacote", "namespacemembers.html", [
        [ "Tudo", "namespacemembers.html", null ],
        [ "Funções", "namespacemembers_func.html", null ],
        [ "Variáveis", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Ficheiros", "files.html", [
      [ "Lista de ficheiros", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"aluguer_8py.html"
];

var SYNCONMSG = 'clique para desativar a sincronização do painel';
var SYNCOFFMSG = 'clique para ativar a sincronização do painel';