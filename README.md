# Instituto Superior de Engenharia da Universidade do Algarve

[![N|Solid](http://ise.ualg.pt/sites/default/files/theme-logos/ise.svg)](https://ise.ualg.pt)

## Projeto Ambientes de Desenvolvimento e Colaboração

## Instruções de Uso
Para correr a aplicação é necessário instalar o módulo tabulate.
##
No terminal escreva:
###
```sh
   pip install tabulate
```

## Funcionalidades

- Registar Nova Trotineta
- Listar Trotinetas
- Registar Novo Utilizador
- Listar Utilizadores
- Registar Alugueres
- Listar Alugueres

## Menu Inicial
###
```sh
    *********************************************************************
    :        A trote - Sistema de gestão de aluguer de trotinetas       : 
    *********************************************************************
    :                                                                   :
    : nt - nova trotineta       lt - lista trotinetas                   :
    : nu - novo utilizador      lu - lista utilizadores                 :
    : na - novo aluguer         la - lista alugueres                    :
    :                                                                   :
    : g - guarda tudo           c - carrega tudo                        :
    :                                                                   :
    : x - sair                                                          :
    :                                                                   :
    *********************************************************************
```

## Ficheiros

- [main.py](main.py)
- [io_terminal.py](io_terminal.py)
- [io_ficheiros.py](io_ficheiros.py)
- [trotinetas.py](trotinetas.py)
- [utilizadores.py](utilizadores.py)
- [aluguer.py](aluguer.py)



## Licença

MIT
SISTEMAS E TECNOLOGIAS DE INFORMAÇÃO, UALG, 2022

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
