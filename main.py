from trotinetas import (
    cria_trotineta,
    imprime_lista_de_trotinetas,
    nome_ficheiro_lista_de_trotinetas
)
from utilizadores import (
    cria_novo_utilizador,
    imprime_lista_de_utilizadores,
    nome_ficheiro_lista_de_utilizadores
)
from aluguer import (
    cria_novo_aluguer,
    imprime_lista_de_alugueres,
    nome_ficheiro_lista_de_alugueres
)
from io_ficheiros import (
    guarda_em_ficheiro,
    le_de_ficheiro
)
from io_terminal import (
    imprime_lista_de_dicionarios
)
import time

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    CYELLOWBG = '\33[43m'
    CBLACK = '\33[30m'
    """ Class cores

        :param bcolors: Define as cores
        :param HEADER: Define HEADER
        :param OKBLUE: Define OKBLUE
        :param OKCYAN: Define OKCYAN
        :param OKGREEN: Define OKGREEN
        :param FAIL: Define FAIL
        :param ENDC: Define ENDC
        :param BOLD: Define BOLD
        :param UNDERLINE: Define UNDERLINE
        :param CYELLOWBG: Define CYELLOWBG
        :param CBLACK: CDefine BLACK
        
        """

titulo = bcolors.CYELLOWBG + bcolors.CBLACK + "A trote - Sistema de gestão de aluguer de trotinetas" + bcolors.ENDC
nt = (bcolors.OKBLUE + "nt" + bcolors.ENDC)
lt = (bcolors.OKBLUE + "lt" + bcolors.ENDC)
nu = (bcolors.WARNING + "nu" + bcolors.ENDC)
lu = (bcolors.WARNING + "lu" + bcolors.ENDC)
na = (bcolors.OKGREEN + "na" + bcolors.ENDC)
la = (bcolors.OKGREEN + "la" + bcolors.ENDC)
g = (bcolors.WARNING + "g" + bcolors.ENDC)
c = (bcolors.OKGREEN + "c" + bcolors.ENDC)
x = (bcolors.FAIL + "x" + bcolors.ENDC)

# cor_o = (bcolors.OKBLUE + "A trote - Sistema de gestão de aluguer de trotinetas" + bcolors.ENDC)
# cor_x = (bcolors.WARNING + "X" + bcolors.ENDC)
# cor_o_x = (bcolors.OKBLUE + "O" + bcolors.ENDC + bcolors.WARNING + "X" + bcolors.ENDC)

def menu():
    # Application main menu

    lista_de_trotinetas = []
    lista_de_utilizadores = []
    lista_de_alugueres = []

    while True:
        print(f"""
        *********************************************************************
        :        {titulo}       : 
        *********************************************************************
        :                                                                   :
        : {nt} - nova trotineta       {lt} - lista trotinetas                   :
        : {nu} - novo utilizador      {lu} - lista utilizadores                 :
        : {na} - novo aluguer         {la} - lista alugueres                    :
        :                                                                   :
        : {g} - guarda tudo           {c} - carrega tudo                        :
        :                                                                   :
        : {x} - sair                                                          :
        :                                                                   :
        *********************************************************************
        """)

        op = input("Escolha uma opção? ").lower()

        if op == "x":
            exit()
        elif op == "nt":
            nova_trotineta = cria_trotineta()
            lista_de_trotinetas.append(nova_trotineta)
        elif op == "lt":
            imprime_lista_de_trotinetas(lista_de_trotinetas)
        elif op == "nu":
            novo_utilizador = cria_novo_utilizador()
            lista_de_utilizadores.append(novo_utilizador)
        elif op == "lu":
            imprime_lista_de_utilizadores(lista_de_utilizadores)
        elif op == "g":
            guarda_as_listas_em_ficheiros(lista_de_trotinetas, lista_de_utilizadores, lista_de_alugueres)
        elif op == "c":
            lista_de_trotinetas, lista_de_utilizadores, lista_de_aluguer = carrega_as_listas_dos_ficheiros()
        elif op == "na":
            if lista_de_utilizadores and lista_de_trotinetas:
                id_utilizador = pergunta_id(questao="Qual o id do comprador? ", lista=lista_de_utilizadores)
                id_trotineta = pergunta_id(questao="Qual o id do veículo? ", lista=lista_de_trotinetas)
                dias = input("Quantos dias de aluguer? ")
                valor = input("Qual o valor? ")
                lista_de_alugua = cria_novo_aluguer(id_utilizador, id_trotineta, dias, valor)
                lista_de_alugueres.append(lista_de_alugua)
            else:
                print("Erro: tem de ter utilizadores e trotinetas")
        elif op == "la":
            imprime_lista_de_alugueres(lista_de_alugueres)
            pass


def pergunta_id(questao, lista):
    """ Questiona qual os dados para o aluguer

    :param questao: faz a questão
    :param lista: mostra a lista
    :return: idx else imprime lista
    """

    imprime_lista_de_dicionarios(lista)
    while True:
        idx = int(input(questao))
        if 0 <= idx < len(lista):
            return idx
        else:
            print(f"id inexistente. Tente de novo. Valores admitidos {0} - {len(lista)}")


def carrega_as_listas_dos_ficheiros():
    """ Carrega a lista dos ficheiros

    :param lista_de_veiculos: carrega a lista
    :param lista_de_utilizadores: carrega a lista
    :param lista_de_alugueres: carrega a lista
    :return: lista_de_veiculos, lista_de_utilizadores, lista_de_alugueres
    """

    lista_de_veiculos = le_de_ficheiro(nome_ficheiro_lista_de_trotinetas)
    lista_de_utilizadores = le_de_ficheiro(nome_ficheiro_lista_de_utilizadores)
    lista_de_alugueres = le_de_ficheiro(nome_ficheiro_lista_de_alugueres)
    return lista_de_veiculos, lista_de_utilizadores, lista_de_alugueres


def guarda_as_listas_em_ficheiros(lista_de_veiculos, lista_de_utilizadores, lista_de_alugueres):
    """ Guarda as listas em ficheiros

    :param lista_de_utilizadores: guarda a lista
    :param lista_de_veiculos: guarda a lista
    :param lista_de_alugueres: guarda a lista
    :return: o que leu das listas
    """

    op = input("Os dados nos ficheiros serão sobrepostos. Continuar (S/N)?")
    if op in ['s', 'S', '']:
        guarda_em_ficheiro(nome_ficheiro_lista_de_trotinetas, lista_de_veiculos)
        guarda_em_ficheiro(nome_ficheiro_lista_de_utilizadores, lista_de_utilizadores)
        guarda_em_ficheiro(nome_ficheiro_lista_de_alugueres, lista_de_alugueres)
    else:
        print("Cancelada.")


if __name__ == "__main__":
    menu()