from io_terminal import imprime_lista

nome_ficheiro_lista_de_alugueres = "lista_de_alugueres.pk"

def cria_novo_aluguer(id_utilizador, id_trotineta, dias, valor):
    """ Pede os dados de um novo aluguer

    :return: dicionario com o id utilizador, id trotineta e numero de dias e valor na forma,
        {"id utilizador": <<id_utilizador>>, "id trotineta": <<id_trotineta>>, "dias": <<dias>>, "valor": <<valor>>}
    """

    #id_utilizador = input("Escolha o id da lista de utilizadores? ")
    #id_trotineta = input("Escolha o id da lista de trotinetes? ")
    #dias = input("Quantos dias de aluguer? ")
    #valor = input("Qual o valor? ")
    return {"id_utilizador": id_utilizador, "id_trotineta": id_trotineta,"dias": dias, "valor": valor, }


def imprime_lista_de_alugueres(lista_de_alugueres):
    """ Imprime lista de alugueres

    :param cabecalho: cabecalho para a listagem
    :param lista: lista a ser impressa
    """

    imprime_lista(cabecalho="Lista de alugueres", lista=lista_de_alugueres)

